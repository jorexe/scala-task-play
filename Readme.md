# Scala task project for Scalac

This project aims to solve the exercise provided by ScalaC interview process.

## Description

GitHub portal is centered around organizations and repositories. Each organization has many
repositories and each repository has many contributors. Your goal is to create an endpoint that given
the name of the organization will return a list of contributors sorted by the number of contributions.

### Constraints

- Use GitHub REST API v3 (https://developer.github.com/v3/)
- Return a list sorted by the number of contributions made by the developer to all repositories for the given
  organization.
- Respond to a GET request at port 8080 and address /org/{org_name}/contributors
- Respond with JSON, which should be a list of contributors in the following format: { “name”:<contributor_login>,
  “contributions”: <no_of_contributions> }
- Handle GitHub’s API rate limit restriction using a token that can be set as an environment variable of name GH_TOKEN
- Friendly hint: GitHub API returns paginated responses. You should take it into account if you want the result to be
  accurate for larger organizations.

### Judging criteria

- Finished working sample (usable API, clear instructions on how to run and use)
- Code quality (idiomatic Scala, functional programming preferred)
- Design quality (proper abstractions)
- Tests
- Taking advantage of the framework in use
- HTTP protocol usage (response codes, headers, caching, etc.)
- Performance
- Documented code (where it’s relevant)

## Required software

- Java 11.0.14
- Scala 2.13.10
- [Sbt](https://www.scala-sbt.org/download.html) 1.7.2

## Running the project

### Development environment

To run the project in development mode run the following command:

```bash
sbt run
```

Then after the `Terminating server binding for /0:0:0:0:0:0:0:0:9000` you should be able to access the http server
in `http://localhost:9000`

Example:

```bash
curl http://localhost:9000/health
```

### Production environment

The application is prepared to run using docker. To build the docker image run the following command:

```bash
sbt Docker/publishLocal 
```

This will generate `scala-task-play` with `1.0-SNAPSHOT` and `latest` tags.

To run dockerized application, you can use the following command:

```bash
docker run --env GH_TOKEN=$GH_TOKEN -p 9000:9000 scala-task-play
```

### Tests

To run the tests run the following command:

```bash
sbt test
```

## Decisions taken

### Play Framework

I've considered using different libraries for the project, including [http4s](https://http4s.org/)
and [sttp](https://sttp.softwaremill.com/).
Although it could be an overkill for the problem presented, I've realized that according to time and effort Play 
Framework was a better alternative because:

- It isn't as focused on low-level APIs
- Contains both HTTP Client and HTTP Server
- Has JSON utils out of the box, without the need to add an external dependency such
  as [circe](https://circe.github.io/circe/)

### Layers

I've decided to keep in the domain the logic of the contributor's aggregation because from my point of view it looked
cleaner, and I needed to fetch all the data for a specific organization anyway.

I was thinking about an optimization fetching pages only when I needed them, but that's not possible if I want to have
accurate results for the number of contributions of each contributor. (A contributor can have 1 contribution in the 
last repository in the last page).

A point that can be improved, is that now I'm returning directly the `domain` in the controller, and we could use 
some DTOs to have more abstraction

### Configuration

I've preferred to let multiple configurations in the configuration files, including the pagination, in case we 
wanted to test how the service behaves with different amounts of elements per page, or the max page size in Github 
is updated (Currently `100`).

### Caching

I've decided to use [Scaffeine](https://github.com/blemale/scaffeine) as cache implementation because it runs on top 
of [Caffeine](https://github.com/ben-manes/caffeine), which is a really good cache and also comes in a plugin in 
Play Framework. Also, I did the service quite generic, so eventually the cache could be upgraded to an external one.

### Error propagation

I've started using the model to propagate errors from the GithubAPI to the domain using the `Either` monad. However, 
it started to be really complicated due to the number of maps to join all the contributors from each repo. I've read 
about using `Monad Transofmers` but sadly I ran out of time to investigate and implement them correctly. Although 
with the current model is working (Throwing an exception), I don't think it is the best solution.

### WS Client

Although I've used the WS client as an Http client, I think that choice could be challenged by a client with more 
configurations. I would like to play to see how the WS client behaves limiting the amount of concurrent http calls and 
tuning [other configurations](https://www.playframework.com/documentation/2.8.x/ScalaWS#Configuring-AsyncHttpClientConfig),
and compare it with other libraries.

### Docker

I've added docker to the project because I find it an easer way to build and run it. I've also tried to build and 
push a docker image in bitbucket without success, because I didn't have time to find a proper image to run the 
pipeline (I've done this before in Gitlab)