package remote.github.util

import scala.util.Try

case class Pagination(prev: Option[Int], next: Option[Int], first: Option[Int], last: Option[Int])

object Pagination {

  type LinkParameters = Map[String, Map[String, String]]

  def getPagination(header: String): Pagination = {
    val parsed = parsePaginationLinks(header)
    Pagination(
      getPage(parsed, "prev"),
      getPage(parsed, "next"),
      getPage(parsed, "first"),
      getPage(parsed, "last")
    )
  }
  private def getPage(parsed: LinkParameters, page: String): Option[Int] =
    parsed.get(page).flatMap(_.get("page")).map(_.toInt)

  private def parsePaginationLinks(header: String): LinkParameters = {
    Try {
      header.split(", ?").map(parsePaginationLink).toMap
    }.getOrElse(Map.empty)
  }

  private def parseRel(rel: String): String =
    rel.replace("rel=\"", "").replace("\"", "")

  private def parseUrlParams(url: String): Map[String, String] =
    url
      .replace("<", "")
      .replace(">", "")
      .split("\\?")(1)
      .split("&")
      .map(_.split("="))
      .map(a => (a(0), a(1)))
      .toMap

  private def parsePaginationLink(link: String): (String, Map[String, String]) = {
    val split = link.split("; ?")
    (parseRel(split(1)), parseUrlParams(split(0)))
  }
}
