package remote.github.api

import play.api.libs.json.Reads
import play.api.libs.ws.WSResponse
import remote.github.util.Pagination

sealed trait GithubApiResponse[T] {
  def combine(other: GithubApiResponse[T]): GithubApiResponse[T]
}

case class Success[T](body: Seq[T], pagination: Pagination) extends GithubApiResponse[T] {
  override def combine(other: GithubApiResponse[T]): GithubApiResponse[T] = other match {
    case Success(otherBody, _) => Success(body ++ otherBody, pagination)
    case Failure(_, _)         => other
  }
}
case class Failure[T](status: Int, error: String) extends GithubApiResponse[T] {
  override def combine(other: GithubApiResponse[T]): GithubApiResponse[T] = this

}

object GithubApiResponse {
  def fromWsResponse[T](response: WSResponse)(implicit fjs: Reads[T]): GithubApiResponse[T] =
    if (response.status == 200) {
      Success(response.json.as[Vector[T]], Pagination.getPagination(response.header("Link").getOrElse("")))
    } else
      Failure(response.status, (response.json \ "message").as[String])
}
