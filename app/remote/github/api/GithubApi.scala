package remote.github.api

import domain.models.GithubApiResponseException
import play.api.Configuration
import play.api.libs.json.Reads
import play.api.libs.ws.WSClient
import remote.github.models.Contributor
import remote.github.models.Repository
import remote.github.util.Pagination

import javax.inject.Inject
import scala.concurrent.ExecutionContext
import scala.concurrent.Future

class GithubApi @Inject() (config: Configuration, ws: WSClient)(implicit ec: ExecutionContext) {

  private val GithubApiUrl: String      = config.get[String]("remote.github.api-url")
  private val GithubApiToken: String    = config.get[String]("remote.github.api-token")
  private val GithubPaginationSize: Int = config.get[Int]("remote.github.pagination-size")

  assert(GithubPaginationSize > 0, "Github pagination size should be greater than 0")

  private def ghRequest[T](path: String, page: Int)(implicit fjs: Reads[T]): Future[GithubApiResponse[T]] =
    ws.url(s"$GithubApiUrl$path")
      .withQueryStringParameters("page" -> page.toString, "per_page" -> GithubPaginationSize.toString)
      .addHttpHeaders("Accept" -> "application/vnd.github+json", "Authorization" -> s"Bearer $GithubApiToken")
      .get()
      .map(r => GithubApiResponse.fromWsResponse(r))

  private def ghPaginatedRequest[T](path: String)(
      implicit fjs: Reads[T]
  ): Future[Seq[T]] = {
    ghRequest(path, 1)
      .flatMap(first =>
        first match {
          case Success(_, Pagination(_, _, _, Some(last))) =>
            Future
              .sequence((2 to last).map(ghRequest(path, _)))
              .map(_.foldLeft(first)(_.combine(_)))
          case _ => Future(first)
        }
      )
      .map {
        case Success(body, _)       => body
        case Failure(status, error) => throw GithubApiResponseException(status, error)
      }
  }

  def getOrgRepos(org: String): Future[Seq[Repository]] =
    ghPaginatedRequest[Repository](s"/orgs/$org/repos")

  def getRepoContributors(owner: String, repo: String): Future[Seq[Contributor]] =
    ghPaginatedRequest[Contributor](s"/repos/$owner/$repo/contributors")

}
