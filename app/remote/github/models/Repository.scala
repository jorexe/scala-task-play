package remote.github.models

import play.api.libs.json.{Json, Reads}

import scala.language.implicitConversions

case class Repository(name: String, id: Long)

object Repository {
  implicit val repositoryReads: Reads[Repository] = Json.reads[Repository]
}
