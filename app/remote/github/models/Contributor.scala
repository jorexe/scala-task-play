package remote.github.models

import play.api.libs.json.Json
import play.api.libs.json.Reads

import scala.language.implicitConversions

case class Contributor(id: Long, login: String, contributions: Long)

object Contributor {
  implicit val contributorReads: Reads[Contributor] = Json.reads[Contributor]

  def toDomain(contributor: Contributor): domain.models.Contributor =
    domain.models.Contributor(contributor.id, contributor.login, contributor.contributions)
}
