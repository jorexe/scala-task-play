package remote.github.services

import com.github.blemale.scaffeine.{AsyncLoadingCache, Scaffeine}
import domain.models.Organization
import domain.services.GithubService
import play.api.Configuration

import javax.inject.Inject
import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration

class GithubCachedHttpService @Inject() (config: Configuration, githubHttpService: GithubHttpService)
    extends GithubService {

  private val CacheExpiration = config.get[FiniteDuration]("remote.github.cache.expiration")
  private val CacheMaxSize    = config.get[Int]("remote.github.cache.max-size")

  /**
   * Cache for organizations with configurable expiration and size
   * Enable {@link Scaffeine#recordStats()} if you want to tune the cache
   */
  val cache: AsyncLoadingCache[String, Organization] = Scaffeine()
    .expireAfterWrite(CacheExpiration)
    .maximumSize(CacheMaxSize)
    .buildAsyncFuture(githubHttpService.getOrganization)

  override def getOrganization(name: String): Future[Organization] = cache.get(name)

}
