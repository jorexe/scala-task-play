package remote.github.services

import domain.models.Organization
import domain.services.GithubService
import remote.github.api.GithubApi
import remote.github.models.Contributor

import javax.inject.Inject
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.language.implicitConversions

class GithubHttpService @Inject() (githubHttp: GithubApi)(implicit ec: ExecutionContext) extends GithubService {

  private def getRepoContributors(org: String, repo: String) =
    githubHttp.getRepoContributors(org, repo).map((repo, _))

  private def getOrganizationContributors(name: String): Future[Map[String, Seq[Contributor]]] =
    for {
      repos        <- githubHttp.getOrgRepos(name)
      contributors <- Future.sequence(repos.map(repo => getRepoContributors(name, repo.name))).map(_.toMap)
    } yield contributors

  override def getOrganization(name: String): Future[Organization] =
    getOrganizationContributors(name).map(c =>
      Organization(name, c.toVector.map(v => domain.models.Repository(v._1, v._2.map(Contributor.toDomain).toVector)))
    )
}
