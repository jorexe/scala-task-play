package domain.services

import domain.models.Organization

import scala.concurrent.Future

trait GithubService {
  def getOrganization(name: String): Future[Organization]

}
