package domain.models

case class Organization(name: String, repositories: Vector[Repository]) {

  def contributors: Vector[Contributor] =
    repositories
      .flatMap(_.contributors)
      .groupMapReduce(_.id)(c => c)(_.combine(_))
      .values
      .toVector
      .sortBy(_.contributions)(Ordering[Long].reverse)
}
