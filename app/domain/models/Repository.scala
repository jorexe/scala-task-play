package domain.models

case class Repository(name: String, contributors: Vector[Contributor])
