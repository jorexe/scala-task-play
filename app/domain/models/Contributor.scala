package domain.models

case class Contributor(id: Long, name: String, contributions: Long) {
  def combine(other: Contributor): Contributor =
    if (id != other.id) throw new RuntimeException("Can't combine different contributors")
    else Contributor(id, name, contributions + other.contributions)

}
