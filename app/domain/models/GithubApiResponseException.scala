package domain.models

case class GithubApiResponseException(status: Int, error: String) extends RuntimeException
