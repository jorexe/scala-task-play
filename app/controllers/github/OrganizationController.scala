package controllers.github

import domain.models.Contributor
import domain.models.GithubApiResponseException
import play.api.libs.json.Json
import play.api.libs.json.OWrites
import play.api.mvc._
import remote.github.services.GithubCachedHttpService

import javax.inject.Inject
import javax.inject.Singleton
import scala.concurrent.ExecutionContext

@Singleton
class OrganizationController @Inject() (
    val controllerComponents: ControllerComponents,
    githubService: GithubCachedHttpService
)(
    implicit ec: ExecutionContext
) extends BaseController {

  implicit val contributorsWrites: OWrites[Contributor] = Json.writes[Contributor]
  def contributors(orgName: String): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    githubService
      .getOrganization(orgName)
      .map(_.contributors)
      .map(Json.toJson[Vector[Contributor]])
      .map(Ok(_))
      .recover {
        case GithubApiResponseException(status, error) => Status(status)(error)
        case e: Exception                              => Status(500)(e.getMessage)
      }
  }

}
