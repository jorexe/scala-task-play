package controllers

import javax.inject._
import play.api._
import play.api.libs.json.Json
import play.api.mvc._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HealthController @Inject() (val controllerComponents: ControllerComponents) extends BaseController {

  /**
   * Create an Action to return health status
   */
  def status(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok(Json.obj("status" -> "ok"))
  }
}
