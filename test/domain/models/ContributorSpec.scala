package domain.models

import org.scalatestplus.play.PlaySpec

class ContributorSpec extends PlaySpec {
  "Contributor" should {
    "combine properly with other contributor with the same id" in {
      val c1 = Contributor(1, "sample", 17)
      val c2 = Contributor(1, "sample", 31)

      c1.combine(c2) mustBe Contributor(1, "sample", 48)
    }

    "throw exception if tries to combine with different ids" in {
      val c1 = Contributor(1, "sample", 17)
      val c2 = Contributor(2, "sample", 31)

      a[RuntimeException] must be thrownBy {
        c1.combine(c2)
      }
    }
  }
}
