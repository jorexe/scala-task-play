package domain.models

import org.scalatestplus.play.PlaySpec

class OrganizationSpec extends PlaySpec {
  "Organization" should {
    "return no contributors" in {
      val org = Organization("org", Vector.empty)

      org.contributors mustBe Vector.empty
    }

    "return contributors from repository" in {
      val contributors = Vector(Contributor(1, "test-1", 12), Contributor(2, "test-2", 9), Contributor(3, "test-3", 7))
      val repository   = Repository("rep1", contributors)
      val org          = Organization("org", Vector(repository))

      org.contributors mustBe contributors
    }

    "return contributors from repository ordered by contributions" in {
      val contributors = Vector(Contributor(1, "test-1", 3), Contributor(2, "test-2", 9), Contributor(3, "test-3", 7))
      val repository   = Repository("rep1", contributors)
      val org          = Organization("org", Vector(repository))

      org.contributors mustBe Vector(
        Contributor(2, "test-2", 9),
        Contributor(3, "test-3", 7),
        Contributor(1, "test-1", 3)
      )
    }

    "combine contributors properly" in {
      val contributors1 = Vector(Contributor(2, "test-2", 3), Contributor(3, "test-3", 7))
      val repository1   = Repository("rep1", contributors1)
      val contributors2 = Vector(Contributor(1, "test-1", 4), Contributor(2, "test-2", 9))
      val repository2   = Repository("rep2", contributors2)
      val contributors3 = Vector(Contributor(1, "test-1", 7))
      val repository3   = Repository("rep3", contributors3)
      val repository4   = Repository("rep4", Vector.empty)

      val org = Organization("org", Vector(repository1, repository2, repository3, repository4))
      org.contributors mustBe Vector(
        Contributor(2, "test-2", 12),
        Contributor(1, "test-1", 11),
        Contributor(3, "test-3", 7)
      )
    }

  }
}
