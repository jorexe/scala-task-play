package controllers

import org.scalatest.TestData
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._

import scala.language.implicitConversions

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 *
 * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
 */
class HealthControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  implicit override def newAppForTest(testData: TestData): Application =
    new GuiceApplicationBuilder().configure(Map("remote.github.api-token" -> "test")).build()

  "HealthController GET" should {

    "return ok status from a new instance of controller" in {
      val controller = new HealthController(stubControllerComponents())
      val health     = controller.status().apply(FakeRequest(GET, "/health"))

      status(health) mustBe OK
      contentType(health) mustBe Some("application/json")
      contentAsJson(health) mustBe Json.obj("status" -> "ok")
    }

    "return ok status from the application" in {
      val controller = inject[HealthController]
      val health     = controller.status().apply(FakeRequest(GET, "/health"))

      status(health) mustBe OK
      contentType(health) mustBe Some("application/json")
      contentAsJson(health) mustBe Json.obj("status" -> "ok")
    }

    "return ok status from the router" in {
      val request = FakeRequest(GET, "/health")
      val health  = route(app, request).get

      status(health) mustBe OK
      contentType(health) mustBe Some("application/json")
      contentAsJson(health) mustBe Json.obj("status" -> "ok")
    }
  }
}
