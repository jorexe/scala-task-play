package remote.github.util

import org.scalatestplus.play.PlaySpec

class PaginationSpec extends PlaySpec {
  "HeaderParser" should {
    "parse last page properly in single element" in {
      val header = "<https://api.github.com/repositories/4578898/contributors?per_page=100&page=5>; rel=\"last\""

      Pagination.getPagination(header) mustBe Pagination(None, None, None, Some(5))
    }

    "parse last page with two elements" in {
      val header =
        "<https://api.github.com/repositories/4578898/contributors?per_page=100&page=2>; rel=\"next\", <https://api.github.com/repositories/4578898/contributors?per_page=100&page=4>; rel=\"last\""

      Pagination.getPagination(header) mustBe Pagination(None, Some(2), None, Some(4))
    }

    "parse last page with multiple elements" in {
      val header =
        "<https://api.github.com/repositories/4578898/contributors?per_page=100&page=1>; rel=\"prev\", <https://api.github.com/repositories/4578898/contributors?per_page=100&page=3>; rel=\"next\", <https://api.github.com/repositories/4578898/contributors?per_page=100&page=11>; rel=\"last\", <https://api.github.com/repositories/4578898/contributors?per_page=100&page=1>; rel=\"first\""

      Pagination.getPagination(header) mustBe Pagination(Some(1), Some(3), Some(1), Some(11))
    }

    "return None if header no present" in {
      val header = ""

      Pagination.getPagination(header) mustBe Pagination(None, None, None, None)
    }
  }
}
